import sqlite3
from os.path import exists


if not exists("../visitors.db"):
    connection = sqlite3.connect('../visitors.db')
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE visitors (ipaddress text, visits integer)")
    connection.commit()


def get_tracked_ips():
    connection = sqlite3.connect('../visitors.db')
    cursor = connection.cursor()
    cursor.execute('SELECT ipaddress FROM visitors')
    results = cursor.fetchall()
    return [result[0] for result in results]
