import json
import time
from datetime import timedelta, datetime
from os import environ
from urllib.parse import urlencode, urljoin

from authlib.integrations.flask_client import OAuth
from flask import Flask, render_template, request, session, redirect, url_for
from pygments import formatters, highlight, lexers

import api_manager
from auth_decorators import requires_auth
from geoip_api import lookup_geoips
from page_render_helpers import build_map
from tracking_wrapper import get_tracked_ips


app = Flask(__name__)
app.secret_key = environ.get("GEOIP_FLASK_SESSION_SECRET")
app.config['SPONSOR_UPLOAD_FOLDER'] = "uploads/sponsor_logos"

oauth = OAuth(app)
auth0 = oauth.register(
    'auth0',
    client_id=environ.get("GEOIP_AUTH_ID"),
    client_secret=environ.get("GEOIP_AUTH_SECRET"),
    api_base_url=environ.get("GEOIP_AUTH_URL"),
    access_token_url=environ.get("GEOIP_AUTH_TOKEN_URL"),
    authorize_url=environ.get("GEOIP_AUTH_VALIDATE_URL"),
    client_kwargs={
        'scope': 'openid profile email',
    },
)

api_manager.get_bearer()


@app.template_filter('ctime')
def timectime(s):
    return time.ctime(s)


@app.route('/login')
def login():
    origin = request.args.get("origin", type=str)
    session['login_origin'] = origin or "sponsor_dashboard"
    return auth0.authorize_redirect(redirect_uri=urljoin(environ.get("GEOIP_ADMIN_URL"), url_for(
        "auth_callback_handling")))


@app.route('/auth_callback')
def auth_callback_handling():
    # Handles response from token endpoint
    auth0.authorize_access_token()
    resp = auth0.get('userinfo')
    userinfo = resp.json()
    session['jwt_payload'] = userinfo
    session['profile'] = {
        'user_id': userinfo['sub'],
        'name': userinfo['name'],
        'picture': userinfo['picture']
    }
    origin = session.get('login_origin') or "dashboard"
    return redirect(url_for(origin))


@app.route('/')
@requires_auth
def dashboard():
    geoip_records = lookup_geoips(get_tracked_ips())
    formatter = formatters.HtmlFormatter(style='lovelace')
    api_response = api_manager.lookup_metrics()
    lookup_sum_1d = sum(lookup["count"] for lookup in api_response["lookedup"] if
                       lookup["timestamp"] > (datetime.now() - timedelta(days=1)).timestamp())
    lookup_sum_7d = sum(lookup["count"] for lookup in api_response["lookedup"] if
                       lookup["timestamp"] > (datetime.now() - timedelta(days=7)).timestamp())
    lookup_sum_30d = sum(lookup["count"] for lookup in api_response["lookedup"] if
                       lookup["timestamp"] > (datetime.now() - timedelta(days=30)).timestamp())
    notfound_1d = [notfound for notfound in api_response["notfound"] if
                       notfound["timestamp"] > (datetime.now() - timedelta(days=1)).timestamp()]
    notfound_7d = [notfound for notfound in api_response["notfound"] if
                       notfound["timestamp"] > (datetime.now() - timedelta(days=7)).timestamp()]
    notfound_30d = [notfound for notfound in api_response["notfound"] if
                       notfound["timestamp"] > (datetime.now() - timedelta(days=30)).timestamp()]
    notfound_sum1d = sum(notfound["count"] for notfound in notfound_1d)
    notfound_sum7d = sum(notfound["count"] for notfound in notfound_7d)
    notfound_sum30d = sum(notfound["count"] for notfound in notfound_30d)
    metrics = {
        "lookups_24hrs": lookup_sum_1d,
        "lookups_7days": lookup_sum_7d,
        "lookups_30days": lookup_sum_30d,
        "notfound_24hrs": notfound_sum1d,
        "notfound_7days": notfound_sum7d,
        "notfound_30days": notfound_sum30d
    }
    metrics = highlight(
        json.dumps(
            metrics,
            sort_keys=True,
            indent=4).encode(),
        lexers.JsonLexer(),
        formatter
    )
    notfound = highlight(
        json.dumps(
            notfound_7d,
            sort_keys=True,
            indent=4).encode(),
        lexers.JsonLexer(),
        formatter
    )
    return render_template('dashboard.html',
                           extended_css=formatter.get_style_defs('.highlight'),
                           map=build_map(geoip_records),
                           notfound=notfound,
                           metrics=metrics,
                           selected_page="dashboard",
                           logout=True)


@app.route('/lookup_cidr', methods=["GET"])
@requires_auth
def lookup_cidr():
    return render_template('lookup.html', selected_page="lookup_cidr", logout=True)


@app.route('/lookup_cidr', methods=["POST"])
@requires_auth
def found_cidr():
    cidr = request.form.get("cidr")
    formatter = formatters.HtmlFormatter(style='lovelace')
    lookup_response = api_manager.lookup_cidr(cidr)
    found = highlight(
        json.dumps(
            lookup_response,
            sort_keys=True,
            indent=4).encode(),
        lexers.JsonLexer(),
        formatter
    )
    return render_template(
        'lookup.html',
        extended_css=formatter.get_style_defs('.highlight'),
        found_cidr=found,
        cidr=lookup_response.get("cidr", cidr),
        create=True if "error" in lookup_response else False,
        selected_page="lookup_cidr",
        logout=True
    )

@app.route('/new_cidr', methods=["GET"])
@requires_auth
def new_cidr():
    return render_template('cidr.html', create=True, selected_page="new_cidr", logout=True)


@app.route('/new_cidr', methods=["POST"])
@requires_auth
def cidr_new():
    cidr = request.form.get("cidr")
    rir = request.form.get("rir")
    cc = request.form.get("cc")
    asn = request.form.get("asn")
    as_name = request.form.get("as_name")
    long = request.form.get("long")
    lat = request.form.get("lat")
    radius = request.form.get("radius")
    jdata = {
      "cidr": cidr,
      "allocated_cc": cc,
      "rir": rir,
      "asn": asn,
      "as-name": as_name,
      "geo": {
        "type": "Feature",
        "properties": {"radius": radius},
        "geometry": {
          "type": "point",
          "coordinates": [
              long,
              lat
          ]
        }
      }
    }
    api_manager.create_cidr(jdata)
    return redirect(url_for("update_cidr", cidr=cidr))


@app.route('/update_cidr/<path:cidr>', methods=["GET"])
@requires_auth
def update_cidr(cidr):
    lookup_response = api_manager.lookup_cidr(cidr)
    return render_template(
        'cidr.html',
        cidr=lookup_response.get("cidr", ""),
        cc=lookup_response.get("allocated_cc", ""),
        rir=lookup_response.get("rir", ""),
        asn=lookup_response.get("asn", ""),
        as_name=lookup_response.get("as-name", ""),
        long=lookup_response.get("geo", {}).get("geometry", {}).get("coordinates", [0.0, 0.0])[0],
        lat=lookup_response.get("geo", {}).get("geometry", {}).get("coordinates", [0.0, 0.0])[1],
        radius=lookup_response.get("geo", {}).get("properties", {}).get("radius", 0.0),
        create=False,
        selected_page="update_cidr",
        logout=True)


@app.route('/update_cidr', methods=["POST"])
@requires_auth
def cidr_update():
    cidr = request.form.get("cidr")
    rir = request.form.get("rir")
    cc = request.form.get("cc")
    asn = request.form.get("asn")
    as_name = request.form.get("as_name")
    long = request.form.get("long")
    lat = request.form.get("lat")
    radius = request.form.get("radius")
    jdata = {
      "cidr": cidr,
      "allocated_cc": cc,
      "rir": rir,
      "asn": asn,
      "as-name": as_name,
      "geo": {
        "type": "Feature",
        "properties": {"radius": radius},
        "geometry": {
          "type": "point",
          "coordinates": [
              long,
              lat
          ]
        }
      }
    }
    api_manager.update_cidr(jdata)
    return redirect(url_for("update_cidr", cidr=cidr))

@app.route('/logout')
def logout():
    # Clear session stored data
    session.clear()
    # Redirect user to logout endpoint
    params = {'returnTo': environ.get("GEOIP_URL"), 'client_id': environ.get("GEOIP_AUTH_ID")}
    return redirect(f"{auth0.api_base_url}v2/logout?{urlencode(params)}")


if __name__ == '__main__':
    app.run()
