import folium


def render_funding_progress(funded, required):
    percentage = funded / required
    green_part = round(500*percentage, 1)
    rect1 = f'<rect width="{green_part}pt" height="25pt" style="fill:#229727" />'
    rect2 = f'<rect x="{green_part}pt" width="{(500 - green_part)}pt" height="25pt" style="fill:#972222" />'
    text1 = f'<text x="180pt" y="18pt" fill="#E5E5E5" font-family="Cutive Mono, monospace" font-weight="normal" font-size="18pt">${funded} / ${required}</text>'
    return f"{rect1}{rect2}{text1}"


def build_map(geoip_records, highlight=None):
    folium_map = folium.Map(location=(0, 0), tiles="OpenStreetMap", zoom_start=1)
    folium.TileLayer('cartodbpositron').add_to(folium_map)
    for record in geoip_records:
        if record is None or "geo" not in record:
            continue
        color = "#985ECE"
        if (highlight is not None) and (
                record["geo"]["geometry"]["coordinates"] == highlight["geo"]["geometry"]["coordinates"]):
            color = "#C516A5"
        folium.GeoJson(record["geo"],
                       name="geojson",
                       marker=folium.Circle(color=color)).add_to(folium_map)
    return folium_map._repr_html_()