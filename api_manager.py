from os import environ
from threading import Timer
from urllib.parse import urljoin

import requests


api_auth = None


def get_bearer():
    global api_auth
    headers = {'content-type': "application/x-www-form-urlencoded"}
    payload = {
        "grant_type": "client_credentials",
        "client_id": environ.get("GEOIP_AUTH_ID"),
        "client_secret": environ.get("GEOIP_AUTH_SECRET"),
        "audience": environ.get("GEOIP_API_URL")
    }
    api_auth = requests.post(urljoin(environ.get("GEOIP_AUTH_URL"), "oauth/token"), headers=headers, data=payload).json()
    Timer(api_auth["expires_in"]-1, get_bearer).start()


def lookup_metrics():
    result = requests.get(
        urljoin(environ.get('GEOIP_API_URL'), 'v1.0/metrics'),
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    result.raise_for_status()
    return result.json()


def lookup_cidr(cidr: str):
    result = requests.get(
        urljoin(environ.get('GEOIP_API_URL'), f'v1.0/cidr/{cidr}/internal'),
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    return result.json()


def create_cidr(jdata: dict):
    result = requests.post(
        urljoin(environ.get('GEOIP_API_URL'), f'v1.0/cidr/{jdata["cidr"]}'),
        json=jdata,
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    return result.json()

def update_cidr(jdata: dict):
    result = requests.put(
        urljoin(environ.get('GEOIP_API_URL'), f'v1.0/cidr/{jdata["cidr"]}'),
        json=jdata,
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    return result.json()
