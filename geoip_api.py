import json
from functools import lru_cache

import requests

def lookup_geoip(public_ip):
	response = requests.get(f"https://api.geoip.network/v1.0/cidr/{public_ip}")
	jdata = None
	if 200 <= response.status_code < 300:
		jdata = response.json()
	return jdata

def lookup_geoips(public_ips):
	response = requests.post(f"https://api.geoip.network/v1.0/cidrs", json=public_ips)
	jdata = None
	if 200 <= response.status_code < 300:
		jdata = response.json()
	return jdata